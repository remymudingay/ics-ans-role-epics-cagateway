import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_carepeater_enabled_and_started(host):
    service = host.service("carepeater")
    assert service.is_running
    assert service.is_enabled


def test_cagateway_enabled_and_started(host):
    service = host.service("cagateway")
    assert service.is_running
    assert service.is_enabled
