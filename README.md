ics-ans-role-epics-cagateway
===================

Ansible role to install epics-cagateway.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
# Host specific variables
# write = all, specific or false
write: specific
client_broadcast: 10.0.2.255
server_ip: 10.0.2.15

# Ansible group or playbook group variables
epics_dir: /etc/gateway
epics_dir_bin: "{{ epics_dir }}/bin"
epics_archive: https://artifactory.esss.lu.se/artifactory/swi-pkg/LXC/epics_channel_gw_3.15.4.tar.gz
epics_dirs:
  - "{{ epics_dir_bin }}"
  - /srv/epics/
gateway_packages:
  - bzip2
  - cscope
  - gettext
  - git
  - indent
  - intltool
  - libstdc++-devel
  - libtool
  - pcre
  - perl
  - pcre-devel
  - perl-devel
  - readline
  - readline-devel
  - systemtap
  - which
  - wget

# Systemd services
channel_access_services:
  - carepeater.service
  - cagateway.service

# Path to binaries
ca_gateway_bin:
  - /srv/epics/bases/extensions/src/gateway/bin/linux-x86_64/gateway
  - /srv/epics/bases/base-3.15.4/bin/linux-x86_64/caRepeater

# User access group and associated users
epics_uag:
  - name: group1
    users:
      - user1
      - user2
  - name: group2
    users:
      - userx
      - usery
# host access group and associated hosts
epics_hag:
  - name: hostgroup
    hosts:
      - workstation1
      - workstation2
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-epics-cagateway
```

License
-------

BSD 2-clause
---
